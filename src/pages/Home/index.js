import { useState } from 'react'; 
import {Header} from '../../components/Header';
import background from '../../assets/background.jpg';
import ItemList from '../../components/ItemList';
import './styles.css'

function App() {
  const [user, setUser] = useState('');
  const [currentUser, setCurrentUser] = useState(null);
  const [repos, setRepos] = useState(null);

  const handleGetData = async () => {
    const urlGit = `https://api.github.com/users/${user}`;
    const userData = await fetch(urlGit);
    if (!userData.ok) {
      const message = `An error has occured: ${userData.status} + ${urlGit}`;
      throw new Error(message);
    }
    // .then(response => response.json())
    // .then(userData => console.log(userData.name))
    // .catch(error => console.log(error));
    const newUser = await userData.json();
    console.log(newUser);

    if(newUser.name){
      const {avatar_url, name, bio, login} = newUser;
      setCurrentUser({avatar_url, name, bio, login});

      const reposData = await fetch(`https://api.github.com/users/${user}/repos`);
      const newRepos = await reposData.json();
      if(newRepos.length){
        setRepos(newRepos);
      }
    }
    console.log(newUser);
  };
  return (
    <div className="App">
      <Header />
      <div className="conteudo">
        <img src={background} className="background" alt="background app"/>
        <div className="info">
          <div>
            <input name='usuario' value={user} onChange={event => setUser(event.target.value)} placeholder='@username' />
            <button onClick={handleGetData}>Buscar</button>
          </div>
          {currentUser?.name ? (<>
            <div className='perfil'>
              <img 
                src={currentUser.avatar_url} 
                className='profile'
                alt='rafael {currentUser.name}'
                />
              <div>
                <h3>{currentUser.name}</h3>
                <span>@{currentUser.login}</span>
                <p>{currentUser.bio}</p>
              </div>
            </div>
            <hr />
          </>): null}
          {repos?.length ?(
          <div>
            <h4 className='repositorio'>Repositorios</h4>
            {repos.map(repo => (
                <ItemList title={repo.name} description={repo.description} />
            ))}
          </div>
          ):null }
        </div>
      </div>
    </div>
  );
}

export default App;
